OBJS = app.o ttt_krl.o
HEADS = ttt_krl.h

CC = gcc
DEBUG = -g3 -O0
AFLAGS = -Wall -Wextra -Wconversion $(DEBUG)
CFLAGS = $(AFLAGS) -c 
LDFLAGS = $(AFLAGS)

all: ex.x

ex.x: $(OBJS)
		$(CC) $(LDFLAGS) -o $@ $^ -lm

%.o: %.c $(HEADS)
		$(CC) $(CFLAGS) $<

.PHONY: clean tidy

tidy:
		rm -rf *.o core

clean: tidy
		rm -rf *.x


