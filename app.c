/*
 * krl: tic tac toe demo
 * v0.1.40
 * 2016-01-10
 */


#include <stdio.h>						// for std use
#include "ttt_krl.h"					// tictactoe funs
#define DEBUG 1			

#define T3S	9							// size of ttt - ToDo: dyn
#define NumberOfactiveSolver 3			// 0 - random
										// 1 - prototype
										// 2 - dummy

int main() {
	int counter = 0;
	char data[T3S+1];					// matrix w/ data[T3S+1] als debug counter
	int i,checkInput;					// LCV & check scanf, it is good old C...
	int solver_num = 1;					// default solver#
	char sign = '-';					// default array value
	bool findWinner = false;
	bool okInput = false;

	for(i = 0; i < T3S; i++) data[i] = sign;

	printf("\nTic-tac-toe (also known as Noughts and crosses or Xs and Os)");
	printf("\n is a paper-and-pencil(or Terminal) game for two players, X and O");
	printf("\nwho take turns marking the spaces in a 3×3 grid(or grater?)");
	printf("\nThe player who succeeds in placing three of their marks:");
	printf("\nIn a horizontal, vertical, or diagonal row wins the game!!");

	/*
	 * ToDo: dyn. size
	 */

	printf("Please choose solver:\n");
	printf("\t1\trandomized -default\n");
	printf("\t2\tfancy ultra evil npc\n");
	printf("\t3\tdummy npc\n");

	do{
		checkInput = scanf("%d", &solver_num);
		fflush(stdin);
		if(checkInput == 1){
			if(solver_num < NumberOfactiveSolver)
			{
				okInput = true;
			}
			else{
				printf("\n\r\tunvalid input, try again\n");
			}
		}
	}while(okInput == false);


	// Main loop. max T3S times
	while (counter < T3S){	
		putGrid_mod(data, T3S, counter); // print grid

		if (counter % 2 == 0) {
			sign = 'X';
			move_player(data, &sign);
		} else {
			sign = 'O';
			//move_player(data, &sign); 		// for PvP mode
			move_npc(data, &sign, &solver_num); // player vs npc w/ solver#
		}
		counter++;
		if(DEBUG) printf("\napp.c counter = %d", counter);

		findWinner = check4winner(data, &sign);
		if(DEBUG) printf("\napp findWinner = %d", findWinner);

		if(findWinner) break;
	}

	putGrid_mod(data, T3S, counter); // alpha

	if (findWinner) {
		printf("%c is the winner. Congrats!!\n", sign);
	} else {
		printf("Match draw.. Best of luck for both\n");
	}
	return 0;
}
