/*        
 * krl: tic tac toe demo
 * source for project v0.1.40
 * * 2016-01-10
 * */ 

#include "ttt_krl.h"
#include <math.h>
#include <stdlib.h>

#define T3S 9
#define DEBUG 1

char player_sign = 'X';
char npc_sign = 'O';

void putGrid_mod(char *data, int grid_size, int curr_round)	// ToDo: dyn size
{
	int i,j,rowcolum = (int)(sqrt(grid_size)), index = 0;
	printf("\nround %d:\n\n", ++curr_round);

	for(i = 0; i < rowcolum; i++){
		printf("\t\t");
		for(j = 0; j < rowcolum; j++)
		{
			printf("| %c ", data[index++]);
		}		printf("|");
		if (i < rowcolum -1) printf("\n\t\t----+---+----");
		printf("\n");
	}
	return;
}

void move_player(char *data, char *sign)
{
	int index = 9; // ToDo: dyn Debug index
	int checkInput;
	bool finish;
	printf("\ncheckpoint alpha\n");
	if (DEBUG){
		printf("\nplayer: %c\n", *sign);
		printf("\n[debug] data9: %d\n", ++data[9]);
	}

	do{
		printf("Move for player [1-9]:\n");
		checkInput = scanf("%d", &index);
		fflush(stdin);

		if(checkInput == 1){
			if (index < 1 || index > T3S) {
				printf("Allowed input is: 1 to %d!!\n", T3S);
				finish = false;
			}   
			else if (data[index - 1] == 'X' || data[index - 1] == 'O') {
				printf("Position is already occupied!!\n");
				finish = false;
			}
			else finish = true;
		}
		else{
			printf("\n\tŧ\n");
		}

		printf("\ncheckpoint alpha-2!\n");
	}while(finish == false);
	data[index - 1] = *sign;
	return;
}

void move_npc(char *data, char *sign, int *solver_num)
{
	if(DEBUG){
		printf("\nnpc: %c, solver: %d\n", *sign,*solver_num);
		printf("\ndebug-data: %d\n", ++data[9]);
	}
	switch (*solver_num){
		case 1:
			if(DEBUG) printf("\nrandom npc");
			randomNPC(data, sign);			
			break;

		case 2:
			printf("\ncheckpoint delta");
			break;

		default:
			printf("\ncheckponint epsilon");
	}

	if(DEBUG) printf("\ncheckpoint gamma\n");
	return;
}

bool check4winner(char *data, char *sign)
{
	int i,j,flag;
	bool findWinner = false;

	for (i = 0; i < T3S; i++) {
		if (i % 3 == 0) flag = 0;
		if (data[i] == *sign) flag++;
		if (flag == 3) findWinner = true;
	}

	flag = 0;
	for (i = 0; i < 3; i++) {
		for (j = i; j <= i + 6; j = j + 3) {
			if (data[j] == *sign) flag++;
		}
		if (flag == 3) findWinner = true;
		flag = 0;
	}
	if ((data[0] == *sign && data[4] == *sign && data[8] == *sign) || (data[2] == *sign && data[4] == *sign && data[6] ==  *sign)) {
		findWinner = true;
	}
	return findWinner;
}


void randomNPC(char *data, char *sign)
{
	int index;
	bool finish;
	printf("\ncheckpoint beta\n");
	do{                                                                                     
		index = (rand()%9) + 1;                 // ToDo: set random seed
		printf("Move for npc: %d\n", index);

		if (index < 1 || index > T3S){
			if(DEBUG) printf("npc -Allowed input is: 1 to %d!!\n", T3S);
			finish = false;
		}   
		else if (data[index - 1] == 'X' || data[index - 1] == 'O'){
			if(DEBUG) printf("npc -Position is already occupied!!\n");
			finish = false;
		}
		else finish = true;

	}while(finish == false);    
	data[index - 1] = *sign;
} 
