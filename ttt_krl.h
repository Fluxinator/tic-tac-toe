/*        
 * krl: tic tac toe demo
 * header for project v0.1.40                      
 * 2016-01-10
 */ 


#ifndef ttt_krl_h
#define ttt_krl_h

#include <stdio.h>
typedef enum {false, true} bool;

void putGrid(char *data);  								//display grid w/ data

void putGrid_mod(char *data, int size, int curr_round); //display grid w/ data

void move_player(char *data, char *sign);				// handel user input

void move_npc(char *data, char *sign, int *solver_num);	// handle npc foo

bool check4winner(char *data, char *sign);				// look for winner

void randomNPC(char *data, char *sign);					// randmom solver for NPC

#endif
